
**SERVER CONFIGURATION**

Install node modules by command npm install
Run project using the command node index.js 
Currently project is running on 3000 port
If you want to change the port, then you can do it through config.js file
MongoDB is running on default port 27017

**API**
1. Admin Login API   api/v1/admin/login
2. AddUser Post API   api/v1/admin/user
3. User List GET API   api/v1/admin/user
4. Update Check In/ Check Out PUT API api/v1/admin/user

APIs 2-4 are secured with JWT Authentication
 



**CLIENT APP**


Admin will login using the credentials

Username: admin@test.com
Password: qwerty

**Steps to run the app**
1. run npm install 
2. Open .env file and set value for REACT_APP_DEV_API_URL (base URL)
3. run npm start

This is a secure login process where I have used JWT Verification.

The JWT token received here after login will be passed to all the REST APIs

If the token gets expired then admin user has to re-login as he/ she won't be able to access anything, not even route.

After login, I have displayed Uers Listing from where admin can check in or check out any users as an event participant

There is a CREATE button in the header from where admin user will be navigated to a page from where he can create a new user.

Added the required validation of empty fields.

Logout button on the top from where admin user can logout from the client app.
