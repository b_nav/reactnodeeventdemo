import * as React from "react";

interface ItemProps {
  firstName: string;
  lastName: string;
  checkin: boolean;
  onChange?: any;
}

const UserItem: React.SFC<ItemProps> = ({
  firstName,
  lastName,
  checkin,
  onChange
}) => {
  return (
    <div className="item">
      {firstName + " " + lastName}
      <button
        className={checkin ? "checked-in" : "checked-out"}
        onClick={onChange}
      >
        {checkin ? "Do Checkout" : "Do Checkin"}
      </button>
    </div>
  );
};
UserItem.defaultProps = {
  firstName: "",
  lastName: "",
  checkin: false
};

export default UserItem;
