import React from "react";
import { API_URL } from "../constant";

export const login = (username: string, password: string) => {
  let url = API_URL + "login";
  let options = {
    body: JSON.stringify({
      username,
      password
    }),
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  };
  return fetch(url, options)
    .then(res => res.json())
    .catch(error => error);
};
