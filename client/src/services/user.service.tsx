import React from "react";
import { parseJwt } from "./jwt";
import { API_URL } from "../constant";
let storage = localStorage;
let authToken = storage.getItem("authToken") || "";

export const saveToken = (token: string) => {
  authToken = token;
  storage.setItem("authToken", authToken);
};

export const hasAccess = () => {
  if (!authToken) return false;
  let data = parseJwt(authToken);
  let exp = data.exp;
  let now = new Date().getTime() / 1000;
  return exp > now;
};

export const getToken = () => {
  return authToken;
};

export const getAll = () => {
  let url = API_URL + "user";
  let options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "x-access-token": "Bearer " + authToken
    }
  };
  return fetch(url, options)
    .then(res => res.json())
    .catch(error => error);
};

export const createNew = (firstname: string, lastname: string) => {
  let url = API_URL + "user";
  let options = {
    body: JSON.stringify({
      firstname,
      lastname,
      checkin: false
    }),
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "x-access-token": "Bearer " + authToken
    }
  };
  return fetch(url, options)
    .then(res => res.json())
    .catch(error => error);
};

export const toggleCheckin = (_id: string, checkin: boolean) => {
  let url = API_URL + "user";
  let options = {
    body: JSON.stringify({
      checkin,
      _id
    }),
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "x-access-token": "Bearer " + authToken
    }
  };
  return fetch(url, options)
    .then(res => res.json())
    .catch(error => error);
};

export const logout = () => {
  storage.clear();
};
