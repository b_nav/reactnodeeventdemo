import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import Dashboard from "./containers/user";
import CreateUser from "./containers/user/create";
import { hasAccess } from "./services/user.service";

const PrivateRoutes: React.FC = () => {
  return (
    <div className="App">
      <Router>
        {hasAccess() ? (
          <Switch>
            <Route exact name="users" path="/user/list" component={Dashboard} />
            <Route
              exact
              name="newuser"
              path="/user/new"
              component={CreateUser}
            />
          </Switch>
        ) : (
          <Redirect from="/" to="/login" />
        )}
      </Router>
    </div>
  );
};

export default PrivateRoutes;
