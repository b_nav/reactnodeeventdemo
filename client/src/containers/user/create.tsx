import * as React from "react";
import { createNew } from "../../services/user.service";
import { FIRST_NAME_VALIDATION, LAST_NAME_VALIDATION } from "../../meassage";
import Field from "../../components/form-field";

interface MyProps {
  history: any;
}

interface MyState {
  firstname: string;
  lastname: string;
  hasError: boolean;
  fname_error: boolean;
  lname_error: boolean;
}

const updateState = <T extends string>(key: string, value: T) => (
  prevState: MyState
): MyState => ({
  ...prevState,
  [key]: value
});

export default class Login extends React.Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      hasError: false,
      fname_error: false,
      lname_error: false
    };
  }

  submit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const { firstname, lastname } = this.state;
    console.log(firstname, lastname);
    createNew(firstname, lastname).then(response => {
      if (response.status) {
        alert(response.msg);
        this.props.history.goBack();
      }
    });
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    let key = e.target.name;
    this.setState(updateState(key, e.target.value));
  }

  handleBlur(e: React.ChangeEvent<HTMLInputElement>) {
    let key = e.target.name;
    const { firstname, lastname } = this.state;
    switch (key) {
      case "firstname":
        this.setState({ fname_error: !firstname });
        break;
      case "lastname":
        this.setState({ lname_error: !lastname });
        break;
      default:
        return;
    }
  }

  hasError() {
    let { fname_error, lname_error } = this.state;
    return fname_error || lname_error;
  }

  render() {
    const { fname_error, lname_error } = this.state;
    return (
      <div>
        <header>
          <h2>Create User</h2>
        </header>
        <form onSubmit={this.submit.bind(this)}>
          <Field
            name="firstname"
            type="text"
            value={this.state.firstname}
            onChange={this.handleChange.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            is_error={fname_error}
            error_message={FIRST_NAME_VALIDATION}
            label="First Name"
          />
          <Field
            name="lastname"
            type="text"
            value={this.state.lastname}
            onChange={this.handleChange.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            is_error={lname_error}
            error_message={LAST_NAME_VALIDATION}
            label="Last Name"
          />

          <button type="submit" disabled={this.hasError()}>
            Submit
          </button>
        </form>
      </div>
    );
  }
}
