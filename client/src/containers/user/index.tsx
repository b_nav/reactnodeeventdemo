import React from "react";
import UserItem from "../../components/user-item";
import { getAll, toggleCheckin, logout } from "../../services/user.service";

interface MyProps {
  history: any;
}

interface MyState {
  users: any;
}
class HomePage extends React.Component<MyProps, MyState> {
  mounted: boolean;
  constructor(props: any) {
    super(props);
    this.state = {
      users: []
    };
    this.createNew = this.createNew.bind(this);
    this.logout = this.logout.bind(this);
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    this.loadList();
  }

  loadList() {
    getAll().then(response => {
      if (response.status) {
        if (this.mounted) this.setState({ users: response.users });
      } else {
        alert(response.msg);
      }
    });
  }

  toggleCheckin(id: string, checkin: boolean) {
    toggleCheckin(id, !checkin).then(response => {
      if (response.status) {
        this.loadList();
      }
    });
  }

  logout() {
    logout();
    console.log(this.props.history);
    this.props.history.go("/");
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  createNew() {
    this.props.history.push("new");
  }
  render() {
    const { users } = this.state;
    return (
      <div>
        <header>
          <h2>User List</h2>
          <div>
            <button onClick={this.createNew}>Create +</button>
            <button onClick={this.logout}>Logout</button>
          </div>
        </header>
        {users.map((user: any, index: any) => (
          <UserItem
            key={index}
            firstName={user.firstname}
            lastName={user.lastname}
            checkin={user.checkin}
            onChange={() => this.toggleCheckin(user._id, user.checkin)}
          />
        ))}
      </div>
    );
  }
}

export default HomePage;
