import * as React from "react";
import { login } from "../../services/login.service";
import { saveToken } from "../../services/user.service";
import { USERNAME_VALIDATION, PASSWORD_VALIDATION } from "../../meassage";
import Field from "../../components/form-field";
interface MyProps {
  history: any;
}

interface MyState {
  username: string;
  password: string;
  hasError: boolean;
  uname_error: boolean;
  password_error: boolean;
}

const updateState = <T extends string>(key: string, value: T) => (
  prevState: MyState
): MyState => ({
  ...prevState,
  [key]: value
});

export default class Login extends React.Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      username: "",
      password: "",
      hasError: false,
      uname_error: false,
      password_error: false
    };
  }

  login(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const { username, password } = this.state;
    console.log(username, password);
    login(username, password).then(response => {
      if (response.status) {
        saveToken(response.token);
        this.props.history.push("/user/list");
      } else {
        alert(response.msg);
      }
      console.log(response);
    });
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    let key = e.target.name;
    this.setState(updateState(key, e.target.value));
  }

  handleBlur(e: React.ChangeEvent<HTMLInputElement>) {
    let key = e.target.name;
    const { username, password } = this.state;
    switch (key) {
      case "username":
        this.setState({ uname_error: !username });
        break;
      case "password":
        this.setState({ password_error: !password });
        break;
      default:
        return;
    }
  }

  hasError() {
    let { uname_error, password_error } = this.state;
    return uname_error || password_error;
  }

  render() {
    const { uname_error, password_error } = this.state;
    return (
      <div>
        <header>
          <h2>Login</h2>
        </header>
        <form onSubmit={this.login.bind(this)}>
          <Field
            name="username"
            type="text"
            value={this.state.username}
            onChange={this.handleChange.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            is_error={uname_error}
            error_message={USERNAME_VALIDATION}
            label="Username"
          />

          <Field
            name="password"
            type="password"
            value={this.state.password}
            onChange={this.handleChange.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            is_error={password_error}
            error_message={PASSWORD_VALIDATION}
            label="Password"
          />

          <button type="submit" disabled={this.hasError()}>
            Submit
          </button>
        </form>
      </div>
    );
  }
}
