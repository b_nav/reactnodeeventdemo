var jwt = require('jsonwebtoken');
var config = require('../config');
var User = require('../models/userschema');
const mongoose = require('mongoose');

var	methods ={};


	methods.generateJwt = function(id){
		var expiry = new Date();
		expiry.setDate(expiry.getDate() + 2);

		return jwt.sign({
			id,
			exp: parseInt(expiry.getTime() / 1000),
			},
			config.secret);
	}

	methods.verifyToken = function(req,res,next) {
		if (req.headers['x-access-token'] && req.headers['x-access-token'].split(' ')[0] === 'Bearer') {
			var token = req.headers['x-access-token'];
			jwt.verify(token.split(' ')[1], config.secret, function(err, decoded) {
				if (err){
					return res.status(200).send({ status: false, msg: 'Failed to authenticate token.' });
				}	
				let _id = decoded.id;
				next(_id)
			});
		}
		else {
			return res.status(200).send({ status: false, msg: 'No token provided' });
		}
	}


module.exports = methods;