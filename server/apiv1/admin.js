const methods = {},
	User = require('../models/userschema'),
	{ check, validationResult,body  } = require('express-validator/check'),
	config = require('../config'),
	Jwtmethods = require('../shared/jwt'),
	mongoose = require('mongoose'),
    passport = require('passport');



methods.adminLogin = (req,res,next) => {
    let admin = {
         'username' : 'admin@test.com',
         'pass' : 'qwerty',
         'id' : 1
    }
    const errors = validationResult(req);
    
	if (!errors.isEmpty()) {
    	return res.status(400).json( {errors:errors.array()});
    }
      
    let payload = req.body;
    if(payload.username === admin.username && payload.password === admin.pass){
        let token = Jwtmethods.generateJwt(admin.id);
        return res.status(200).json({ status:true , token});
    }
    else {
        return res.status(200).json({ status:false ,msg:"wrong username or password"});
    }
}

//  add user
methods.addUser =  (user,req,res,next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
    	return res.status(config.errorCodes.validation).json( {errors:errors.array()});
  	}
    let payload = req.body;
  	var newUser = new User({
		firstname:payload.firstname,
		lastname:payload.lastname,
		checkin: payload.checkin,
	});

	newUser.save(function(err){
		if(err){
			return res.status(200).json({ status:false ,msg:"Error in saving userdata. Please try again",err});
		}
		else{
			return res.status(200).json({"status":true, "msg":"user added succesfully."})
		}
	});	
}

methods.getUserList =  (user,req,res,next) => {
    User.find({}, function(err, users){
        if (err){
            return res.send({"status":false, "msg":"Error",err});
        }
        else{
            res.send({"status":true,users});
        }
    });
}

methods.updateUser = (user,req,res,next) => {
    let payload = req.body;
    let userid = mongoose.Types.ObjectId(payload._id);

    User.updateOne({"_id":userid},{$set:{"checkin":payload.checkin}}, function(err, result){
        if (err) {
            return res.status(200).json({"status":false, "msg":"Error in updating user" ,err});
        }
        else{
            return res.status(200).json({"status":true });
        }
    });
}

module.exports = methods;