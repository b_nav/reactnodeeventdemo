var express = require("express");
var router = express.Router();
var adminController = require("../apiv1/admin");
var Jwtmethods = require("../shared/jwt");
const { body } = require("express-validator/check");

/*add user*/
router.post(
  "/login",
  [body("username").exists(), body("password").exists()],
  adminController.adminLogin
);

/*add user*/
router.post(
  "/user",
  [
    body("firstname").exists(),
    body("lastname").exists(),
    body("checkin").exists(),
    Jwtmethods.verifyToken
  ],
  adminController.addUser
);

router.get("/user", Jwtmethods.verifyToken, adminController.getUserList);

/*update user*/
router.put(
  "/user",
  [
    body("id").exists(),
    body("checkin").exists(),
    Jwtmethods.verifyToken
  ],
  adminController.updateUser
);
  

module.exports = router;
